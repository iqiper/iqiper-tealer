//! `iqiper-tealer` is a lib to handle getting tokens from an IdP.
//!
//! ## The tealor actor
//!
//! The tealor actor will provide a simple object to fetch tokens from an IdP.
//! It'll perform token refresh and issuing.

mod error;
mod reqwest_utils;
mod tealer;
mod tealer_actor;
mod tealer_store;

#[cfg(test)]
mod tests;

pub use error::IqiperTealerError;
pub use oauth2;
pub use openidconnect as oidc;
pub use reqwest_utils::{openid_reqwest_client_async, reqwest_client_build_async};
pub use tealer::IqiperTealer;
pub use tealer_actor::{
    CoreIqiperTealerActor, IqiperTealerActor, IqiperTealerActorAuthorizationCodeGrant,
    IqiperTealerActorClientCredentialGrant, IqiperTealerActorPasswordGrant, IqiperTealerGrant,
};
pub use tealer_store::{IqiperTealerId, IqiperTealerStore, IqiperTealerToken};
