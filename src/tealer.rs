use crate::error::IqiperTealerError;
use crate::reqwest_utils::openid_reqwest_client_async;
use crate::tealer_actor::IqiperTealerGrant;
use crate::tealer_store::{IqiperTealerId, IqiperTealerStore, IqiperTealerToken};
use getset::{Getters, MutGetters};
use std::path::Path;

#[derive(Clone, Debug, Getters, MutGetters)]
#[getset(get = "pub")]
pub struct IqiperTealer {
    /// Used to store the tokens
    #[getset(get_mut = "pub")]
    store: IqiperTealerStore,
    /// The client to used to communicate with the IdP
    oidc_client: openidconnect::core::CoreClient,
    /// The metadata used to build the oidc_client
    oidc_metadata: openidconnect::core::CoreProviderMetadata,
    /// The reqwest client the `oidc_client` will be using
    reqwest_client: reqwest::Client,
    /// The type of grant to use for auth
    grant_type: IqiperTealerGrant,
}

impl IqiperTealer {
    /// Fetch metadata from the IdP
    async fn get_oidc_metadata(
        idp_url: openidconnect::IssuerUrl,
        reqwest_client: reqwest::Client,
    ) -> Result<openidconnect::core::CoreProviderMetadata, IqiperTealerError> {
        let discovery_url = idp_url
            .join(".well-known/openid-configuration")
            .map_err(openidconnect::DiscoveryError::UrlParse)?;

        let discovery_document = serde_json::from_slice::<openidconnect::core::CoreProviderMetadata>(
            &*reqwest_client
                .get(discovery_url)
                .header("Accept", "application/json")
                .send()
                .await?
                .error_for_status()?
                .bytes()
                .await?,
        )
        .map_err(openidconnect::DiscoveryError::Parse)?;
        Ok(
            openidconnect::JsonWebKeySet::fetch_async(discovery_document.jwks_uri(), move |req| async move {
                openid_reqwest_client_async(reqwest_client, req).await
            })
            .await
            .map(|jwks| discovery_document.set_jwks(jwks))?,
        )
    }

    /// Create a tealer using the `ClientCredential` grant
    pub async fn new_client_grant(
        reqwest_client: reqwest::Client,
        idp_url: openidconnect::IssuerUrl,
        client_id: openidconnect::ClientId,
        client_secret: Option<openidconnect::ClientSecret>,
    ) -> Result<Self, IqiperTealerError> {
        let oidc_metadata = IqiperTealer::get_oidc_metadata(idp_url, reqwest_client.clone()).await?;
        let oidc_client =
            openidconnect::Client::from_provider_metadata(oidc_metadata.clone(), client_id, client_secret);
        Ok(IqiperTealer {
            store: IqiperTealerStore::new(),
            oidc_client,
            oidc_metadata,
            reqwest_client,
            grant_type: IqiperTealerGrant::ClientGrant,
        })
    }

    /// Create a tealer using the `Password` grant
    pub async fn new_direct_grant(
        reqwest_client: reqwest::Client,
        idp_url: openidconnect::IssuerUrl,
        client_id: openidconnect::ClientId,
        client_secret: Option<openidconnect::ClientSecret>,
    ) -> Result<Self, IqiperTealerError> {
        let oidc_metadata = IqiperTealer::get_oidc_metadata(idp_url, reqwest_client.clone()).await?;
        let oidc_client =
            openidconnect::Client::from_provider_metadata(oidc_metadata.clone(), client_id, client_secret);
        Ok(IqiperTealer {
            store: IqiperTealerStore::new(),
            oidc_client,
            oidc_metadata,
            reqwest_client,
            grant_type: IqiperTealerGrant::DirectGrant,
        })
    }

    /// Create a tealer using the `Authorization Code` grant
    pub async fn new_authorization_code_grant(
        reqwest_client: reqwest::Client,
        idp_url: openidconnect::IssuerUrl,
        redirect_url: openidconnect::RedirectUrl,
        client_id: openidconnect::ClientId,
        client_secret: Option<openidconnect::ClientSecret>,
    ) -> Result<Self, IqiperTealerError> {
        let oidc_metadata = IqiperTealer::get_oidc_metadata(idp_url, reqwest_client.clone()).await?;
        let oidc_client =
            openidconnect::Client::from_provider_metadata(oidc_metadata.clone(), client_id, client_secret)
                .set_redirect_uri(redirect_url.clone());
        Ok(IqiperTealer {
            store: IqiperTealerStore::new(),
            oidc_client,
            oidc_metadata,
            reqwest_client,
            grant_type: IqiperTealerGrant::AuthorizationCodeGrant,
        })
    }

    /// Add a token to the store
    pub fn add_token(
        &mut self,
        token: IqiperTealerToken,
        scopes: Vec<openidconnect::Scope>,
        user: Option<String>,
    ) -> Option<IqiperTealerToken> {
        self.store.insert(IqiperTealerId::new(scopes, user), token)
    }

    /// Get a token from the store
    pub fn get_token(&self, scopes: &Vec<openidconnect::Scope>, user: Option<String>) -> Option<&IqiperTealerToken> {
        self.store.get(&IqiperTealerId::new(scopes.clone(), user))
    }

    /// Remove a token from the store
    pub fn remove_token(
        &mut self,
        scopes: &Vec<openidconnect::Scope>,
        user: Option<String>,
    ) -> Option<IqiperTealerToken> {
        self.store.remove(&IqiperTealerId::new(scopes.clone(), user))
    }

    /// Deserialize the store from file
    pub fn store_from_file(&mut self, path: &Path) -> Result<(), IqiperTealerError> {
        Ok(self.store = IqiperTealerStore::from_file(path)?)
    }

    /// Save the store to a file
    pub fn store_to_file(&self, path: &Path) -> Result<(), IqiperTealerError> {
        Ok(self.store.to_file(path)?)
    }
}
