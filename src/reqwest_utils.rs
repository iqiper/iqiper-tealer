use crate::error::IqiperTealerError;
use std::str::FromStr;
/// Build a default reqwest blocking client.
pub fn reqwest_client_build_async() -> Result<reqwest::Client, IqiperTealerError> {
    Ok(reqwest::Client::builder()
        .redirect(reqwest::redirect::Policy::none())
        .build()?)
}

/// Translate an `openidconnect` request to a `reqwest` request
/// TODO: Could use a bit of optimization
pub async fn openid_reqwest_client_async(
    client: reqwest::Client,
    request: openidconnect::HttpRequest,
) -> Result<openidconnect::HttpResponse, Box<IqiperTealerError>> {
    let mut header_map = reqwest::header::HeaderMap::new();
    for (key, val) in request.headers.into_iter() {
        if key.is_none() {
            continue;
        }
        let key = key.unwrap();
        header_map.insert(
            reqwest::header::HeaderName::from_str(key.as_str())
                .map_err(|err| Box::new(IqiperTealerError::from(err)))?,
            reqwest::header::HeaderValue::from_str(val.to_str().map_err(|err| Box::new(IqiperTealerError::from(err)))?)
                .map_err(|err| Box::new(IqiperTealerError::from(err)))?,
        );
    }
    let res = client
        .request(
            reqwest::Method::from_str(request.method.as_str()).map_err(|err| Box::new(IqiperTealerError::from(err)))?,
            request.url,
        )
        .body(request.body)
        .headers(header_map)
        .send()
        .await
        .map_err(|err| Box::new(IqiperTealerError::from(err)))?;
    let mut header_map = openidconnect::http::HeaderMap::new();
    for (key, val) in res.headers().into_iter() {
        header_map.insert(
            openidconnect::http::header::HeaderName::from_str(key.as_str())
                .map_err(|err| Box::new(IqiperTealerError::from(err)))?,
            openidconnect::http::header::HeaderValue::from_str(
                val.to_str().map_err(|err| Box::new(IqiperTealerError::from(err)))?,
            )
            .map_err(|err| Box::new(IqiperTealerError::from(err)))?,
        );
    }
    Ok(openidconnect::HttpResponse {
        status_code: openidconnect::http::StatusCode::from_u16(res.status().as_u16())
            .map_err(|err| Box::new(IqiperTealerError::from(err)))?,
        body: res
            .bytes()
            .await
            .map_err(|err| Box::new(IqiperTealerError::from(err)))?
            .to_vec(),
        headers: header_map,
    })
}
