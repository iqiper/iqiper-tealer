use crate::error::IqiperTealerError;
use chrono::{DateTime, Duration, Utc};
use getset::Getters;
use openidconnect::core::CoreTokenResponse;
use openidconnect::OAuth2TokenResponse;
use serde::{Deserialize, Serialize};
use std::collections::BTreeMap;
use std::fs::File;
use std::io::{BufReader, BufWriter};
use std::path::Path;
use std::string::ToString;
#[derive(Clone, Debug, Deserialize, Serialize, Getters)]
#[getset(get = "pub")]
pub struct IqiperTealerToken {
    /// The token return by the `openidconnect` crate
    token: CoreTokenResponse,
    /// The time at which that token was received, useful to calculate when it'll expire
    ts: DateTime<Utc>,
}

impl IqiperTealerToken {
    /// Create a new token, providing a the token return by the `openidconnect` crate
    pub fn new(token: CoreTokenResponse) -> Self {
        IqiperTealerToken { token, ts: Utc::now() }
    }

    /// Create a new token, providing a the token return by the `openidconnect` crate
    pub fn new_with_ts(token: CoreTokenResponse, ts: DateTime<Utc>) -> Self {
        IqiperTealerToken { token, ts }
    }

    /// Chech if a token has expired. The leeway is to prevent `TOCTOU` type of errors
    pub fn check_exp_with_leeway(&self, leeway: Duration) -> bool {
        if self.token.expires_in().is_none() {
            return false;
        }
        let saved_duration = Duration::from_std(self.token.expires_in().unwrap());
        if saved_duration.is_err() {
            return true;
        }
        let saved_duration = saved_duration.unwrap();
        self.ts + saved_duration - leeway <= Utc::now()
    }

    /// Chech if a token has expired.
    pub fn check_exp(&self) -> bool {
        self.check_exp_with_leeway(Duration::seconds(0))
    }
}

#[derive(Clone, Debug, Eq, PartialEq, Hash, Deserialize, Serialize, Getters)]
#[getset(get = "pub")]
pub struct IqiperTealerId {
    /// The scope that were requested for this token
    scopes: Vec<openidconnect::Scope>,
    /// The user for which this token was requested
    user: Option<String>,
}

impl IqiperTealerId {
    /// Create a new token id based on the requested scopes and user
    pub fn new(scopes: Vec<openidconnect::Scope>, user: Option<String>) -> Self {
        IqiperTealerId { scopes, user }
    }

    /// Create a new token id based on an actual token and the user for which it was requested
    pub fn from_token(token: &IqiperTealerToken, user: Option<String>) -> Self {
        IqiperTealerId {
            scopes: token.token().scopes().cloned().unwrap_or(vec![]),
            user,
        }
    }
}

impl ToString for IqiperTealerId {
    /// Print a token to string. First the user if any, then a "|", then the
    /// scope list, space separated
    fn to_string(&self) -> String {
        format!(
            "{}|{}",
            self.user().as_ref().unwrap_or(&String::from("")),
            self.scopes()
                .into_iter()
                .map(|x| x.to_string())
                .collect::<Vec<String>>()
                .join(" ")
        )
    }
}

#[derive(Clone, Debug, Deserialize, Serialize, Getters)]
#[getset(get = "pub")]
pub struct IqiperTealerStore {
    /// The store of tokens
    store: BTreeMap<String, IqiperTealerToken>,
}

impl IqiperTealerStore {
    /// Create a new, empty store
    pub fn new() -> Self {
        IqiperTealerStore { store: BTreeMap::new() }
    }

    /// Import a store from file
    pub fn from_file(path: &Path) -> Result<Self, IqiperTealerError> {
        let file = File::open(path)?;
        let reader = BufReader::new(file);
        let store: BTreeMap<String, IqiperTealerToken> = serde_json::from_reader(reader)?;

        Ok(IqiperTealerStore { store })
    }

    /// Export a store to file
    pub fn to_file(&self, path: &Path) -> Result<(), IqiperTealerError> {
        let file = File::create(path)?;
        let writer = BufWriter::new(file);
        Ok(serde_json::to_writer_pretty(writer, self.store())?)
    }

    /// Get a token from the token providing its id
    pub fn get(&self, id: &IqiperTealerId) -> Option<&IqiperTealerToken> {
        self.store.get(&id.to_string())
    }

    /// Get a mutable reference of a token providing its id
    pub fn get_mut(&mut self, id: &IqiperTealerId) -> Option<&mut IqiperTealerToken> {
        self.store.get_mut(&id.to_string())
    }

    /// Add a new token to the store
    pub fn insert(&mut self, id: IqiperTealerId, token: IqiperTealerToken) -> Option<IqiperTealerToken> {
        self.store.insert(id.to_string(), token)
    }

    /// Remove a token from the store
    pub fn remove(&mut self, id: &IqiperTealerId) -> Option<IqiperTealerToken> {
        self.store.remove(&id.to_string())
    }
}
