use crate::error::IqiperTealerError;
use crate::reqwest_utils::openid_reqwest_client_async;
use crate::tealer::IqiperTealer;
use crate::tealer_store::IqiperTealerToken;
use async_trait::async_trait;
use getset::{Getters, Setters};
use openidconnect::OAuth2TokenResponse;

#[derive(PartialEq, Clone, Debug)]
pub enum IqiperTealerGrant {
    AuthorizationCodeGrant,
    ClientGrant,
    DirectGrant,
}

/// The tealer actor trait is to unify how the actor will behave. The actor is
/// the front struct to communicate with the IdP to fetch and refresh tokens
#[async_trait]
pub trait IqiperTealerActor: Send + Sync {
    /// Get the underlying tealer struct
    fn tealer(&self) -> &IqiperTealer;
    /// Get the underlying tealer struct (mutable)
    fn tealer_mut(&mut self) -> &mut IqiperTealer;
    /// Get the grant type set for this actor
    fn get_grant_type(&self) -> &IqiperTealerGrant;
    /// Refresh a token
    async fn refresh_token(&self, token: &IqiperTealerToken) -> Result<IqiperTealerToken, IqiperTealerError>;
}

#[async_trait]
pub trait IqiperTealerActorPasswordGrant: IqiperTealerActor {
    /// Get a new token with direct grant
    async fn get_token_via_password(
        &self,
        username: &openidconnect::ResourceOwnerUsername,
        password: &openidconnect::ResourceOwnerPassword,
        scope: &Vec<openidconnect::Scope>,
    ) -> Result<IqiperTealerToken, IqiperTealerError>;

    /// Try to find a cached token, or refresh an expired one before getting new token from
    /// the IdP. This function will save any new token that is fetched
    async fn try_get_token_via_password(
        &mut self,
        username: &openidconnect::ResourceOwnerUsername,
        password: &openidconnect::ResourceOwnerPassword,
        scope: &Vec<openidconnect::Scope>,
    ) -> Result<IqiperTealerToken, IqiperTealerError>;
}

#[async_trait]
pub trait IqiperTealerActorClientCredentialGrant: IqiperTealerActor {
    /// Get a new token via client grant
    async fn get_token_via_client_credential(
        &self,
        scope: &Vec<openidconnect::Scope>,
    ) -> Result<IqiperTealerToken, IqiperTealerError>;

    /// Try to find a cached token, or refresh an expired one before getting new token from
    /// the IdP. This function will save any new token that is fetched
    async fn try_get_token_via_client_credential(
        &mut self,
        scope: &Vec<openidconnect::Scope>,
    ) -> Result<IqiperTealerToken, IqiperTealerError>;
}

#[async_trait]
pub trait IqiperTealerActorAuthorizationCodeGrant: IqiperTealerActor {
    /// Get a new token via authorization code
    async fn get_token_via_code(
        &self,
        code: &openidconnect::AuthorizationCode,
        state: Option<&openidconnect::CsrfToken>,
    ) -> Result<IqiperTealerToken, IqiperTealerError>;
}
#[derive(Clone, Debug, Getters, Setters)]
pub struct CoreIqiperTealerActor {
    tealer: IqiperTealer,
    #[getset(get = "pub", set = "pub")]
    leeway: chrono::Duration,
}

#[async_trait]
impl IqiperTealerActorClientCredentialGrant for CoreIqiperTealerActor {
    /// Exchange a the client credentials for a client token
    async fn get_token_via_client_credential(
        &self,
        scopes: &Vec<openidconnect::Scope>,
    ) -> Result<IqiperTealerToken, IqiperTealerError> {
        let mut req = self.tealer.oidc_client().exchange_client_credentials();
        for scope in scopes.iter().cloned() {
            req = req.add_scope(scope);
        }
        let reqwest_ref = self.tealer.reqwest_client().clone();
        Ok(req
            .request_async(move |req| async move { openid_reqwest_client_async(reqwest_ref, req).await })
            .await
            .map(|t| IqiperTealerToken::new(t))?)
    }

    /// Exchange a the client credentials for a client token (Returns None if it fails)
    async fn try_get_token_via_client_credential(
        &mut self,
        scopes: &Vec<openidconnect::Scope>,
    ) -> Result<IqiperTealerToken, IqiperTealerError> {
        let tealer = self.tealer();
        let token = match tealer.get_token(scopes, None) {
            Some(token) => match token.check_exp_with_leeway(*self.leeway()) {
                true => match self.refresh_token(&token).await {
                    Ok(token) => Ok(token),
                    Err(_x) => self.get_token_via_client_credential(scopes).await,
                },
                false => Ok(token.clone()),
            },
            None => self.get_token_via_client_credential(scopes).await,
        }?;
        drop(tealer);
        self.tealer_mut().add_token(token.clone(), scopes.clone(), None);
        Ok(token)
    }
}

impl CoreIqiperTealerActor {
    pub fn new(tealer: IqiperTealer) -> Self {
        CoreIqiperTealerActor {
            tealer,
            leeway: chrono::Duration::seconds(0),
        }
    }
}

#[async_trait]
impl IqiperTealerActor for CoreIqiperTealerActor {
    fn tealer(&self) -> &IqiperTealer {
        &self.tealer
    }

    fn tealer_mut(&mut self) -> &mut IqiperTealer {
        &mut self.tealer
    }

    fn get_grant_type(&self) -> &IqiperTealerGrant {
        self.tealer().grant_type()
    }

    /// Exchange the refresh token for a new tokens
    async fn refresh_token(&self, token: &IqiperTealerToken) -> Result<IqiperTealerToken, IqiperTealerError> {
        let reqwest_ref = self.tealer.reqwest_client().clone();
        match token.token().refresh_token() {
            Some(token) => Ok(self
                .tealer
                .oidc_client()
                .exchange_refresh_token(token)
                .request_async(move |req| async move { openid_reqwest_client_async(reqwest_ref, req).await })
                .await
                .map(|t| IqiperTealerToken::new(t))?),
            None => Err(IqiperTealerError::NoRefreshToken),
        }
    }
}

// impl IqiperTealerActorClientCredentialGrant for CoreIqiperTealerActor {
//     fn get_token_via_client_credential(
//         &self,
//         scopes: &Vec<openidconnect::Scope>,
//     ) -> Result<IqiperTealerToken, IqiperTealerError> {
//         let mut req = self.tealer.oidc_client().exchange_client_credentials();
//         match scopes {
//             Some(scopes) => {
//                 for scope in scopes.into_iter().cloned() {
//                     req = req.add_scope(scope);
//                 }
//             }
//             None => (),
//         };
//         Ok(req
//             .request(|req| openid_reqwest_client_sync(self.tealer.reqwest_client(), req))
//             .map(|t| IqiperTealerToken::new(t))?)
//     }
// }

#[async_trait]
impl IqiperTealerActorPasswordGrant for CoreIqiperTealerActor {
    /// Get a new token using the password grant
    async fn get_token_via_password(
        &self,
        username: &openidconnect::ResourceOwnerUsername,
        password: &openidconnect::ResourceOwnerPassword,
        scopes: &Vec<openidconnect::Scope>,
    ) -> Result<IqiperTealerToken, IqiperTealerError> {
        let mut req = self.tealer.oidc_client().exchange_password(&username, &password);
        for scope in scopes.iter().cloned() {
            req = req.add_scope(scope);
        }
        let reqwest_ref = self.tealer.reqwest_client().clone();
        Ok(req
            .request_async(move |req| async move { openid_reqwest_client_async(reqwest_ref, req).await })
            .await
            .map(|t| IqiperTealerToken::new(t))?)
    }

    /// Get a new token using the password grant (or None)
    async fn try_get_token_via_password(
        &mut self,
        username: &openidconnect::ResourceOwnerUsername,
        password: &openidconnect::ResourceOwnerPassword,
        scopes: &Vec<openidconnect::Scope>,
    ) -> Result<IqiperTealerToken, IqiperTealerError> {
        let tealer = self.tealer();
        let token = match tealer.get_token(scopes, Some(username.to_string())) {
            Some(token) => match token.check_exp_with_leeway(*self.leeway()) {
                true => match self.refresh_token(&token).await {
                    Ok(token) => Ok(token),
                    Err(_x) => self.get_token_via_password(username, password, scopes).await,
                },
                false => Ok(token.clone()),
            },
            None => self.get_token_via_password(username, password, scopes).await,
        }?;
        drop(tealer);
        self.tealer_mut()
            .add_token(token.clone(), scopes.clone(), Some(username.to_string()));
        Ok(token)
    }
}
