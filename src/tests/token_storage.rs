use super::*;
use crate::tealer_store::*;
use chrono::Duration;

#[test]
fn token_new() {
    let mut oidc_token = gen_token("access_token1", Some("refresh_token1"), Some(Duration::days(1)));
    let scopes = gen_scopes(vec!["toto", "tutu", "titi", "tata"]);
    oidc_token.set_scopes(Some(scopes));
    let token = IqiperTealerToken::new(oidc_token);
    let mut store = IqiperTealerStore::new();
    let token_id = IqiperTealerId::from_token(&token, None);
    let res = store.insert(token_id.clone(), token.clone());
    assert_eq!(res.is_none(), true, "The wasn't a token with the same key before");
    let got_token = store.get(&token_id);
    assert_eq!(got_token.is_some(), true, "There is a token");
    let got_token = got_token.unwrap();
    assert_eq!(got_token.ts(), token.ts(), "The token share the same timestamp");
    assert_eq!(
        got_token.token().access_token().secret(),
        token.token().access_token().secret(),
        "The token share the same access_token"
    );
}

#[test]
fn token_get_mut() {
    let mut oidc_token = gen_token("access_token1", Some("refresh_token1"), Some(Duration::days(1)));
    let oidc_token2 = gen_token("access_token2", Some("refresh_token2"), Some(Duration::days(1)));
    let scopes = gen_scopes(vec!["toto", "tutu", "titi", "tata"]);
    oidc_token.set_scopes(Some(scopes));
    let token = IqiperTealerToken::new(oidc_token);
    let token2 = IqiperTealerToken::new(oidc_token2);
    let mut store = IqiperTealerStore::new();
    let token_id = IqiperTealerId::from_token(&token, None);
    store.insert(token_id.clone(), token.clone());
    let got_token = store.get_mut(&token_id);
    assert_eq!(got_token.is_some(), true, "There is a token");
    let got_token = got_token.unwrap();
    *got_token = token2;
    let got_token = store.get(&token_id);
    assert_eq!(got_token.is_some(), true, "There is a token");
    let got_token = got_token.unwrap();
    let got_token2 = store.get(&token_id);
    assert_eq!(got_token2.is_some(), true, "There is a token");
    let got_token2 = got_token2.unwrap();
    assert_eq!(
        got_token2.token().access_token().secret(),
        got_token.token().access_token().secret(),
        "They share the same secret"
    );
}

#[test]
fn token_remove() {
    let mut oidc_token = gen_token("access_token1", Some("refresh_token1"), Some(Duration::days(1)));
    let scopes = gen_scopes(vec!["toto", "tutu", "titi", "tata"]);
    oidc_token.set_scopes(Some(scopes));
    let token = IqiperTealerToken::new(oidc_token);
    let mut store = IqiperTealerStore::new();
    let token_id = IqiperTealerId::from_token(&token, None);
    store.insert(token_id.clone(), token.clone());
    store.remove(&token_id.clone());
    let res = store.get(&token_id);
    assert_eq!(res.is_none(), true, "The token has been removed");
}

#[test]
fn token_save_and_read_to_file() {
    let tmp_file = mktemp::Temp::new_file().expect("Can't create temp file");
    let mut oidc_token = gen_token("access_token1", Some("refresh_token1"), Some(Duration::days(1)));
    let scopes = gen_scopes(vec!["toto", "tutu", "titi", "tata"]);
    oidc_token.set_scopes(Some(scopes));
    let token = IqiperTealerToken::new(oidc_token);
    let mut store = IqiperTealerStore::new();
    let token_id = IqiperTealerId::from_token(&token, None);
    store.insert(token_id.clone(), token.clone());
    store
        .to_file(tmp_file.to_path_buf().as_path())
        .expect("To write to temp file");
    let store2 = IqiperTealerStore::from_file(tmp_file.to_path_buf().as_path()).expect("To write to temp file");
    let got_token = store2.get(&token_id);
    assert_eq!(got_token.is_some(), true, "There is a token");
    let got_token = got_token.unwrap();
    assert_eq!(
        got_token.token().access_token().secret(),
        token.token().access_token().secret(),
        "They share the same secret"
    );
}
