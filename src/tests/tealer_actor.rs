use super::*;
use crate::error::IqiperTealerError;
use crate::tealer::*;
use crate::tealer_actor::*;
use crate::tealer_store::*;

#[tokio::test]
async fn tealer_actor_grant_type() {
    let idp_config = get_idp_config();
    let tealer_actor = CoreIqiperTealerActor::new(
        IqiperTealer::new_client_grant(
            reqwest::Client::new(),
            idp_config.url,
            idp_config.client_id,
            Some(idp_config.client_secret),
        )
        .await
        .expect("To discover the IdP"),
    );
    let grant_type = tealer_actor.get_grant_type();
    assert_eq!(
        grant_type,
        &IqiperTealerGrant::ClientGrant,
        "It should be the client grant"
    );
}

#[tokio::test]
async fn tealer_get_token_password_grant() {
    let idp_config = get_idp_config();
    let tealer_actor = CoreIqiperTealerActor::new(
        IqiperTealer::new_direct_grant(
            reqwest::Client::new(),
            idp_config.url,
            idp_config.client_id,
            Some(idp_config.client_secret),
        )
        .await
        .expect("To discover the IdP"),
    );
    tealer_actor
        .get_token_via_password(&idp_config.username.clone(), &idp_config.password.clone(), &vec![])
        .await
        .expect("The token");
}

#[tokio::test]
async fn tealer_get_refresh_grant() {
    let idp_config = get_idp_config();
    let tealer_actor = CoreIqiperTealerActor::new(
        IqiperTealer::new_direct_grant(
            reqwest::Client::new(),
            idp_config.url,
            idp_config.client_id,
            Some(idp_config.client_secret),
        )
        .await
        .expect("To discover the IdP"),
    );
    let token = tealer_actor
        .get_token_via_password(&idp_config.username.clone(), &idp_config.password.clone(), &vec![])
        .await
        .expect("The token");
    let new_token = tealer_actor.refresh_token(&token).await.expect("to refresh the token");
    assert_ne!(
        token.token().access_token().secret(),
        new_token.token().access_token().secret(),
        "token shouldn't match"
    );
    assert_eq!(
        token.token().refresh_token().is_some(),
        true,
        "There should be a refresh token"
    );
    assert_eq!(
        new_token.token().refresh_token().is_some(),
        true,
        "There should be a refresh token"
    );
    assert_ne!(
        token.token().refresh_token().unwrap().secret(),
        new_token.token().refresh_token().unwrap().secret(),
        "token shouldn't match"
    );
}

#[tokio::test]
async fn tealer_get_refresh_no_refresh_token() {
    let idp_config = get_idp_config();
    let mut tealer_actor = CoreIqiperTealerActor::new(
        IqiperTealer::new_direct_grant(
            reqwest::Client::new(),
            idp_config.url,
            idp_config.client_id,
            Some(idp_config.client_secret),
        )
        .await
        .expect("To discover the IdP"),
    );
    let oidc_token = gen_token("access_token1", None, Some(Duration::seconds(10)));
    let token = IqiperTealerToken::new(oidc_token);
    tealer_actor.tealer_mut().add_token(token, vec![], None);
    let token = tealer_actor.tealer().get_token(&vec![], None).expect("The token");
    let err = tealer_actor
        .refresh_token(token)
        .await
        .expect_err("This shouldn't work as there is no refresh");
    if let IqiperTealerError::NoRefreshToken = err {
    } else {
        panic!("The error type is wrong")
    }
}

#[tokio::test]
async fn tealer_get_client_credential_grant() {
    let idp_config = get_idp_config();
    let tealer_actor = CoreIqiperTealerActor::new(
        IqiperTealer::new_client_grant(
            reqwest::Client::new(),
            idp_config.url,
            idp_config.client_id,
            Some(idp_config.client_secret),
        )
        .await
        .expect("To discover the IdP"),
    );
    tealer_actor
        .get_token_via_client_credential(&vec![])
        .await
        .expect("The token");
}

#[tokio::test]
async fn tealer_try_get_client_credential_grant() {
    let idp_config = get_idp_config();
    let mut tealer_actor = CoreIqiperTealerActor::new(
        IqiperTealer::new_client_grant(
            reqwest::Client::new(),
            idp_config.url,
            idp_config.client_id,
            Some(idp_config.client_secret),
        )
        .await
        .expect("To discover the IdP"),
    );
    tealer_actor
        .try_get_token_via_client_credential(&vec![])
        .await
        .expect("The token");
    let token = tealer_actor
        .tealer()
        .store()
        .get(&IqiperTealerId::new(vec![], None))
        .expect("The token");
    let token = IqiperTealerToken::new_with_ts(token.token().clone(), token.ts().clone() - chrono::Duration::days(10));
    tealer_actor
        .tealer_mut()
        .store_mut()
        .insert(IqiperTealerId::new(vec![], None), token);
    tealer_actor
        .try_get_token_via_client_credential(&vec![])
        .await
        .expect("The token");
    tealer_actor
        .tealer_mut()
        .store_mut()
        .remove(&IqiperTealerId::new(vec![], None));
    tealer_actor
        .try_get_token_via_client_credential(&vec![])
        .await
        .expect("The token");
}

#[tokio::test]
async fn tealer_try_get_password_grant() {
    let idp_config = get_idp_config();
    let mut tealer_actor = CoreIqiperTealerActor::new(
        IqiperTealer::new_client_grant(
            reqwest::Client::new(),
            idp_config.url,
            idp_config.client_id,
            Some(idp_config.client_secret),
        )
        .await
        .expect("To discover the IdP"),
    );
    tealer_actor
        .try_get_token_via_password(&idp_config.username, &idp_config.password, &vec![])
        .await
        .expect("The token");
    let token = tealer_actor
        .tealer()
        .store()
        .get(&IqiperTealerId::new(vec![], Some(idp_config.username.to_string())))
        .expect("The token");
    let token = IqiperTealerToken::new_with_ts(token.token().clone(), token.ts().clone() - chrono::Duration::days(10));
    tealer_actor.tealer_mut().store_mut().insert(
        IqiperTealerId::new(vec![], Some(idp_config.username.to_string())),
        token,
    );
    tealer_actor
        .try_get_token_via_password(&idp_config.username, &idp_config.password, &vec![])
        .await
        .expect("The token");
    tealer_actor
        .tealer_mut()
        .store_mut()
        .remove(&IqiperTealerId::new(vec![], Some(idp_config.username.to_string())));
    tealer_actor
        .try_get_token_via_password(&idp_config.username, &idp_config.password, &vec![])
        .await
        .expect("The token");
}
