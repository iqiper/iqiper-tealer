use super::*;
use crate::tealer::*;
use crate::tealer_store::*;
use chrono::Duration;

#[tokio::test]
async fn tealer_auth_code_grant() {
    let idp_config = get_idp_config();
    IqiperTealer::new_authorization_code_grant(
        reqwest::Client::new(),
        idp_config.url,
        openidconnect::RedirectUrl::new(String::from("http://localhost:8000/auth_code"))
            .expect("Correct redirection URL"),
        idp_config.client_id,
        Some(idp_config.client_secret),
    )
    .await
    .expect("To discover the IdP");
}

#[tokio::test]
async fn tealer_client_credential_grant() {
    let idp_config = get_idp_config();
    IqiperTealer::new_client_grant(
        reqwest::Client::new(),
        idp_config.url,
        idp_config.client_id,
        Some(idp_config.client_secret),
    )
    .await
    .expect("To discover the IdP");
}

#[tokio::test]
async fn tealer_password_grant() {
    let idp_config = get_idp_config();
    IqiperTealer::new_direct_grant(
        reqwest::Client::new(),
        idp_config.url,
        idp_config.client_id,
        Some(idp_config.client_secret),
    )
    .await
    .expect("To discover the IdP");
}

#[tokio::test]
async fn tealer_add_token() {
    let idp_config = get_idp_config();
    let mut oidc_token = gen_token("access_token1", Some("refresh_token1"), Some(Duration::seconds(10)));
    let scopes = gen_scopes(vec!["toto", "tutu", "titi", "tata"]);
    oidc_token.set_scopes(Some(scopes.clone()));
    let token = IqiperTealerToken::new(oidc_token);
    let mut tealer = IqiperTealer::new_client_grant(
        reqwest::Client::new(),
        idp_config.url,
        idp_config.client_id,
        Some(idp_config.client_secret),
    )
    .await
    .expect("To discover the IdP");
    tealer.add_token(token.clone(), scopes.clone(), None);
    let got_token = tealer.get_token(&scopes, None);
    assert_eq!(got_token.is_some(), true, "There should be a token");
    let got_token = got_token.unwrap();
    assert_eq!(
        got_token.token().access_token().secret(),
        token.token().access_token().secret(),
        "Secrets don't match"
    );
}

#[tokio::test]
async fn tealer_remove_token() {
    let idp_config = get_idp_config();
    let mut oidc_token = gen_token("access_token1", Some("refresh_token1"), Some(Duration::seconds(10)));
    let scopes = gen_scopes(vec!["toto", "tutu", "titi", "tata"]);
    oidc_token.set_scopes(Some(scopes.clone()));
    let token = IqiperTealerToken::new(oidc_token);
    let mut tealer = IqiperTealer::new_client_grant(
        reqwest::Client::new(),
        idp_config.url,
        idp_config.client_id,
        Some(idp_config.client_secret),
    )
    .await
    .expect("To discover the IdP");
    tealer.add_token(token.clone(), scopes.clone(), None);
    tealer.remove_token(&scopes.clone(), None);
    let got_token = tealer.get_token(&scopes, None);
    assert_eq!(got_token.is_none(), true, "There shouldn't be a token");
}

#[tokio::test]
async fn token_save_and_read_to_file() {
    let idp_config = get_idp_config();
    let tmp_file = mktemp::Temp::new_file().expect("Can't create temp file");
    let mut oidc_token = gen_token("access_token1", Some("refresh_token1"), Some(Duration::days(1)));
    let scopes = gen_scopes(vec!["toto", "tutu", "titi", "tata"]);
    oidc_token.set_scopes(Some(scopes.clone()));
    let token = IqiperTealerToken::new(oidc_token);
    let mut tealer = IqiperTealer::new_client_grant(
        reqwest::Client::new(),
        idp_config.url.clone(),
        idp_config.client_id.clone(),
        Some(idp_config.client_secret.clone()),
    )
    .await
    .expect("To discover the IdP");
    tealer.add_token(token.clone(), scopes.clone(), None);
    tealer
        .store_to_file(tmp_file.to_path_buf().as_path())
        .expect("To write to temp file");
    let mut tealer2 = IqiperTealer::new_client_grant(
        reqwest::Client::new(),
        idp_config.url,
        idp_config.client_id,
        Some(idp_config.client_secret),
    )
    .await
    .expect("To discover the IdP");
    tealer2
        .store_from_file(tmp_file.to_path_buf().as_path())
        .expect("To import the store");
    let got_token = tealer2.get_token(&scopes, None);
    assert_eq!(got_token.is_some(), true, "There is a token");
    let got_token = got_token.unwrap();
    assert_eq!(
        got_token.token().access_token().secret(),
        token.token().access_token().secret(),
        "They share the same secret"
    );
}
