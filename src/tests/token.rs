use super::*;
use crate::tealer_store::*;
use chrono::{Duration, Utc};

#[test]
fn token_id_new_no_scopes_no_user() {
    let token_id = IqiperTealerId::new(vec![], None);
    assert_eq!(token_id.to_string(), "|");
}

#[test]
fn token_id_new_scopes_no_user() {
    let scopes = gen_scopes(vec!["toto", "tutu", "titi", "tata"]);
    let token_id = IqiperTealerId::new(scopes, None);
    assert_eq!(token_id.to_string(), "|toto tutu titi tata");
}

#[test]
fn token_id_new_scopes_user() {
    let scopes = gen_scopes(vec!["toto", "tutu", "titi", "tata"]);
    let token_id = IqiperTealerId::new(scopes, Some(String::from("user1")));
    assert_eq!(token_id.to_string(), "user1|toto tutu titi tata");
}

#[test]
fn token_new() {
    let oidc_token = gen_token("access_token1", Some("refresh_token1"), Some(Duration::days(1)));
    let date_before = Utc::now();
    let token = IqiperTealerToken::new(oidc_token);
    let date_after = Utc::now();
    assert_eq!(
        token.token().access_token().secret(),
        "access_token1",
        "Tokens don't match"
    );
    assert_eq!(
        token.ts() >= &date_before,
        true,
        "Start date is before what it should be"
    );
    assert_eq!(token.ts() <= &date_after, true, "Start date is after what it should be");
    assert_eq!(token.check_exp(), false, "Token shouldn't be expired");
}

#[test]
fn token_new_with_timestamp() {
    let oidc_token = gen_token("access_token1", Some("refresh_token1"), Some(Duration::days(1)));
    let date = Utc::now();
    let token = IqiperTealerToken::new_with_ts(oidc_token, date);
    assert_eq!(
        token.token().access_token().secret(),
        "access_token1",
        "Tokens don't match"
    );
    assert_eq!(*token.ts(), date, "The timestamps should match");
    assert_eq!(token.check_exp(), false, "Token shouldn't be expired");
}

#[test]
fn token_expired() {
    let oidc_token = gen_token("access_token1", Some("refresh_token1"), Some(Duration::seconds(1)));
    let token = IqiperTealerToken::new(oidc_token);
    std::thread::sleep(std::time::Duration::from_millis(1200));
    assert_eq!(token.check_exp(), true, "Token should be expired");
}

#[test]
fn token_no_expiry() {
    let oidc_token = gen_token("access_token1", Some("refresh_token1"), None);
    let token = IqiperTealerToken::new(oidc_token);
    assert_eq!(token.check_exp(), false, "Token shouldn't be expired");
}

#[test]
fn token_expiry_overflow() {
    let mut oidc_token = gen_token("access_token1", Some("refresh_token1"), None);
    oidc_token.set_expires_in(Some(&std::time::Duration::from_secs(u64::MAX)));
    let token = IqiperTealerToken::new(oidc_token);
    assert_eq!(token.check_exp(), true, "Token should be expired");
}

#[test]
fn token_expired_with_leeway() {
    let oidc_token = gen_token("access_token1", Some("refresh_token1"), Some(Duration::minutes(25)));
    let token = IqiperTealerToken::new(oidc_token);
    assert_eq!(
        token.check_exp_with_leeway(Duration::hours(1)),
        true,
        "Token should be expired"
    );
}

#[test]
fn token_id_from_token() {
    let mut oidc_token = gen_token("access_token1", Some("refresh_token1"), Some(Duration::seconds(10)));
    let scopes = gen_scopes(vec!["toto", "tutu", "titi", "tata"]);
    oidc_token.set_scopes(Some(scopes));
    let token = IqiperTealerToken::new(oidc_token);
    let token_id = IqiperTealerId::from_token(&token, Some(String::from("USER1")));
    assert_eq!(
        token_id.to_string(),
        "USER1|toto tutu titi tata",
        "Token id don't match"
    );
}
