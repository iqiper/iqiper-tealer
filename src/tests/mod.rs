mod tealer;
mod tealer_actor;
mod token;
mod token_storage;
use chrono::Duration;
use openidconnect::OAuth2TokenResponse;

pub fn gen_token(
    access_token: &str,
    refresh_token: Option<&str>,
    expiry: Option<Duration>,
) -> openidconnect::core::CoreTokenResponse {
    let mut token = openidconnect::core::CoreTokenResponse::new(
        openidconnect::AccessToken::new(access_token.to_string()),
        openidconnect::core::CoreTokenType::Bearer,
        openidconnect::core::CoreIdTokenFields::new(None, openidconnect::EmptyExtraTokenFields {}),
    );
    token.set_refresh_token(refresh_token.map(|x| openidconnect::RefreshToken::new(x.to_string())));
    token.set_expires_in(
        expiry
            .map(|x| x.to_std().expect("The duration to be std compatible"))
            .as_ref(),
    );
    token
}

pub fn gen_scopes(scopes: Vec<&str>) -> Vec<openidconnect::Scope> {
    scopes
        .into_iter()
        .map(|x| openidconnect::Scope::new(x.to_string()))
        .collect()
}

pub struct IdPConfig {
    url: openidconnect::IssuerUrl,
    client_id: openidconnect::ClientId,
    client_secret: openidconnect::ClientSecret,
    username: openidconnect::ResourceOwnerUsername,
    password: openidconnect::ResourceOwnerPassword,
}

pub fn get_idp_config() -> IdPConfig {
    IdPConfig {
        url: openidconnect::IssuerUrl::new(
            std::env::var("IQIPER_TEALER_URL").expect("The `IQIPER_TEALER_URL` env var to be set"),
        )
        .expect("The issuer URL to be correct"),
        client_id: openidconnect::ClientId::new(
            std::env::var("IQIPER_TEALER_CLIENT_ID").expect("The `IQIPER_TEALER_CLIENT_ID` env var to be set"),
        ),
        client_secret: openidconnect::ClientSecret::new(
            std::env::var("IQIPER_TEALER_CLIENT_SECRET").expect("The `IQIPER_TEALER_CLIENT_ID` env var to be set"),
        ),
        username: openidconnect::ResourceOwnerUsername::new(
            std::env::var("IQIPER_TEALER_USERNAME").expect("The `IQIPER_TEALER_USERNAME` env var to be set"),
        ),
        password: openidconnect::ResourceOwnerPassword::new(
            std::env::var("IQIPER_TEALER_PASSWORD").expect("The `IQIPER_TEALER_PASSWORD` env var to be set"),
        ),
    }
}
