use std::fmt;

#[derive(Debug)]
pub enum IqiperTealerError {
    /// I/O Errors
    IO(std::io::Error),
    /// Url related errors
    URL(url::ParseError),
    /// Serialization/Deserialization related issues
    Serde(serde_json::Error),
    /// Http client related errors
    Reqwest(reqwest::Error),
    /// Error relative to the discovery of the IdP
    OICDiscovery(openidconnect::DiscoveryError<Box<IqiperTealerError>>),
    /// Error when some required piece of information is missing
    MissingInformation(String),
    /// Error while fetching tokens
    TokenRequest(
        openidconnect::RequestTokenError<
            Box<IqiperTealerError>,
            openidconnect::StandardErrorResponse<openidconnect::core::CoreErrorResponseType>,
        >,
    ),
    /// Error when trying to parse a badly formatted header name
    InvalidHeaderName(reqwest::header::InvalidHeaderName),
    /// Error when trying to parse a badly formatted header value
    InvalidHeaderValue(reqwest::header::InvalidHeaderValue),
    /// Error when trying to transform a header to str representation
    HeaderToStr(reqwest::header::ToStrError),
    /// Error when parsing an invalid status code
    InvalidStatusCode(openidconnect::http::status::InvalidStatusCode),
    /// Error when parsing an invalid method
    InvalidMethod(openidconnect::http::method::InvalidMethod),
    /// Error when trying to refresh a token with no refresh token
    NoRefreshToken,
}

impl std::error::Error for IqiperTealerError {}

impl fmt::Display for IqiperTealerError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            IqiperTealerError::IO(io_err) => write!(f, "{}", io_err),
            IqiperTealerError::Reqwest(err) => write!(f, "{}", err),
            IqiperTealerError::URL(err) => write!(f, "{}", err),
            IqiperTealerError::Serde(err) => write!(f, "{}", err),
            IqiperTealerError::OICDiscovery(err) => write!(f, "{}", err),
            IqiperTealerError::TokenRequest(err) => write!(f, "{}", err),
            IqiperTealerError::MissingInformation(err) => write!(f, "Missing {}", err),
            IqiperTealerError::InvalidHeaderName(err) => write!(f, "Header name error : {}", err),
            IqiperTealerError::InvalidHeaderValue(err) => write!(f, "Header value error : {}", err),
            IqiperTealerError::HeaderToStr(err) => write!(f, "Header -> str error : {}", err),
            IqiperTealerError::InvalidStatusCode(err) => write!(f, "Invalid status code : {}", err),
            IqiperTealerError::InvalidMethod(err) => write!(f, "Invalid method : {}", err),
            IqiperTealerError::NoRefreshToken => write!(f, "No refresh token available"),
        }
    }
}

impl From<std::io::Error> for IqiperTealerError {
    fn from(err: std::io::Error) -> Self {
        IqiperTealerError::IO(err)
    }
}

impl From<reqwest::Error> for IqiperTealerError {
    fn from(err: reqwest::Error) -> Self {
        IqiperTealerError::Reqwest(err)
    }
}

impl From<serde_json::Error> for IqiperTealerError {
    fn from(err: serde_json::Error) -> Self {
        IqiperTealerError::Serde(err)
    }
}

impl From<url::ParseError> for IqiperTealerError {
    fn from(err: url::ParseError) -> Self {
        IqiperTealerError::URL(err)
    }
}

impl From<openidconnect::DiscoveryError<Box<IqiperTealerError>>> for IqiperTealerError {
    fn from(err: openidconnect::DiscoveryError<Box<IqiperTealerError>>) -> Self {
        match err {
            openidconnect::DiscoveryError::Request(e) => *e,
            _ => IqiperTealerError::OICDiscovery(err),
        }
    }
}
impl
    From<
        openidconnect::RequestTokenError<
            Box<IqiperTealerError>,
            openidconnect::StandardErrorResponse<openidconnect::core::CoreErrorResponseType>,
        >,
    > for IqiperTealerError
{
    fn from(
        err: openidconnect::RequestTokenError<
            Box<IqiperTealerError>,
            openidconnect::StandardErrorResponse<openidconnect::core::CoreErrorResponseType>,
        >,
    ) -> Self {
        match err {
            openidconnect::RequestTokenError::Request(e) => *e,
            _ => IqiperTealerError::TokenRequest(err),
        }
    }
}

impl From<reqwest::header::InvalidHeaderName> for IqiperTealerError {
    fn from(err: reqwest::header::InvalidHeaderName) -> Self {
        IqiperTealerError::InvalidHeaderName(err)
    }
}

impl From<reqwest::header::InvalidHeaderValue> for IqiperTealerError {
    fn from(err: reqwest::header::InvalidHeaderValue) -> Self {
        IqiperTealerError::InvalidHeaderValue(err)
    }
}

impl From<reqwest::header::ToStrError> for IqiperTealerError {
    fn from(err: reqwest::header::ToStrError) -> Self {
        IqiperTealerError::HeaderToStr(err)
    }
}

impl From<openidconnect::http::status::InvalidStatusCode> for IqiperTealerError {
    fn from(err: openidconnect::http::status::InvalidStatusCode) -> Self {
        IqiperTealerError::InvalidStatusCode(err)
    }
}

impl From<openidconnect::http::method::InvalidMethod> for IqiperTealerError {
    fn from(err: openidconnect::http::method::InvalidMethod) -> Self {
        IqiperTealerError::InvalidMethod(err)
    }
}
